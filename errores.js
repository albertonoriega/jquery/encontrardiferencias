let contadorErrores = 4;
let pulsado1 = true;
let pulsado2 = true;
let pulsado3 = true;
let pulsado4 = true;
let timer = 29;
document.querySelector('span').innerHTML = contadorErrores;

$(function () {
    //Hasta que no se pulsa comenzar no empieza el juego
    $(".comenzar").click(function () {

        // Hacemos que quite segundos al timer
        setInterval(function () {
            $('span.timer').text(timer);
            timer--;
            // Si el timer es menor que 10 cambiamos el color
            if (timer < 10) {
                $('span.timer').css('color', 'red');
            }
            // si se acaba el tiempo mensaje y carga la página de nuevo
            if (timer == -1) {
                alert('Game Over');
                location.reload();
            }
        }, 1000);

        // Si se pulsa el error 1
        $(".error1").click(function () {
            // Hacemos visible la imagen del tick verde
            $('.error1 img').css('visibility', 'visible');
            // Evitamos que descuente otro error si pinchas el mismo error
            if (pulsado1) {
                pulsado1 = false;
                contadorErrores--;
                $('.numeroErorres').text(contadorErrores);
            }
        });

        $(".error2").click(function () {
            $('.error2 img').css('visibility', 'visible');
            if (pulsado2) {
                pulsado2 = false;
                contadorErrores--;
                $('.numeroErorres').text(contadorErrores);
            }
        });

        $(".error3").click(function () {
            $('.error3 img').css('visibility', 'visible');
            if (pulsado3) {
                pulsado3 = false;
                contadorErrores--;
                $('.numeroErorres').text(contadorErrores);
            }
        });

        $(".error4").click(function () {
            $('.error4 img').css('visibility', 'visible');
            if (pulsado4) {
                pulsado4 = false;
                contadorErrores--;
                $('.numeroErorres').text(contadorErrores);
            }
        });

        // Si se encuentran todos los resultados
        $(".errores").click(function () {

            if (contadorErrores == 0) {
                alert('¡Enhorabuena, has ganado!');
                location.reload();
            }
        });
    });


});

